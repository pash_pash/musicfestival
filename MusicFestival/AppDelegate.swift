//
//  AppDelegate.swift
//  MusicFestival
//
//  Created by Pasha on 5/31/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().barTintColor = UIColor(red: 51.0 / 255.0, green: 51.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
        MFLocationManager.sharedInstance?.updateData()
        GMSServices.provideAPIKey(GM_API_KEY)

        self.setupUpdateTimer()
        return true
    }
    
    func setupUpdateTimer() {
        _ = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(AppDelegate.update), userInfo: nil, repeats: true)
    }
    
    @objc func update() {
        MFLocationManager.sharedInstance?.updateData()
    }

}

