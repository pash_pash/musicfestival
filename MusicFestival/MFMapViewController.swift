//
//  MFMapViewController.swift
//  MusicFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps


class MFMapViewController: MFBaseVC, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var modeSwitcher: UISegmentedControl!
    var markers:[GMSMarker]! = []

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: Setup
    internal func setup() {
        self.setupMap()
        self.setupModeSwitcher()
    }
    
    internal func setupMap() {
        self.mapView.delegate = self
        if let locations = (MFLocationManager.sharedInstance?.locations) {
            self.showAnnotationsWithLocations(locations: locations)
        }
        self.focusMapToShowAllMarkers()
    }
    
    internal func setupModeSwitcher() {
        self.modeSwitcher.clipsToBounds = true
        self.modeSwitcher.layer.cornerRadius = 15 //whatever
        self.modeSwitcher.layer.borderWidth = 1
        self.modeSwitcher.layer.borderColor = self.modeSwitcher.tintColor.cgColor
    }
    
    //MARK: Internal
    internal func showAnnotationsWithLocations(locations: [MFLocation]) {
        
        self.mapView.clear()
        self.markers.removeAll()
        for location: MFLocation in locations {
            if (location.latlng != nil) {
                if (location.latlng.lat != nil && location.latlng.lng != nil) {
                    let marker = MFMarker()
                    marker.position = CLLocationCoordinate2D(latitude: location.latlng.lat, longitude: location.latlng.lng)
                    marker.map = self.mapView
                    marker.location = location
                    self.markers.append(marker)
                }
            }
        }
    }
    
    func focusMapToShowAllMarkers() {
        if self.markers.count < 1 {
            return
        }
        let myLocation: CLLocationCoordinate2D = self.markers.first!.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
        
        for marker in self.markers {
            bounds = bounds.includingCoordinate(marker.position)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 15.0))
        }
    }
    
    internal func showAnotations() {
        var locations: [MFLocation]
        let modeSwitcher: UISegmentedControl = self.modeSwitcher//sender as! UISegmentedControl
        if modeSwitcher.selectedSegmentIndex == MFMapMode.MM_ALL.rawValue { // All
            locations = (MFLocationManager.sharedInstance?.locations)!
        } else if modeSwitcher.selectedSegmentIndex == MFMapMode.MM_LIVE.rawValue { // Live
            locations = (MFLocationManager.sharedInstance?.liveLocations)!
        } else if modeSwitcher.selectedSegmentIndex == MFMapMode.MM_30MIN.rawValue { // 30 min
            locations = (MFLocationManager.sharedInstance?.thirtyMinLocations)!
        } else {
            locations = (MFLocationManager.sharedInstance?.locations)!
        }
        self.showAnnotationsWithLocations(locations: locations)
        self.focusMapToShowAllMarkers()
    }
    
    //MARK: IBActions
    @IBAction func didSwitchMode(_ sender: Any) {
        self.showAnotations()
    }
    
    
    //MARK: GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let mfMarker = marker as? MFMarker {
            if mfMarker.location != nil {
                MFLocationManager.sharedInstance?.selectedLocation = mfMarker.location
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "participantDetailVC") as! MFParticipantDetailViewController
                viewController.mode = .SM_LOCATION
                self.navigationController?.pushViewController(viewController, animated: true);
            }
            
        }
        return true
    }
    
    // MARK: - Notification handlers
    override func didUpdatData() {
        DispatchQueue.main.async {
            self.showAnotations()
        }
    }

}
