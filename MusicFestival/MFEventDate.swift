//
//  MFEventDate.swift
//  MusicFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper

class MFEventDate: NSObject, Mappable {
    var startDate:Date!
    var endDate:Date!
    
    var startDateString:String!
    var startTimeString:String!
    var startTimeShortString:String!
    var endDateString:String!
    var endTimeString:String!
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
        
    func mapping(map: Map) {
        startDateString     <- map["StartDate"]
        startTimeString     <- map["StartTime"]
        endDateString     <- map["EndDate"]
        endTimeString     <- map["FinishTime"]
        
        if startDateString != nil && startTimeString != nil && endDateString != nil && endTimeString != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.startDate = dateFormatter.date(from: (startDateString + " " + startTimeString))
            self.endDate = dateFormatter.date(from: (endDateString + " " + endTimeString))
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
            let substr = String(startTimeString.prefix(5))
            self.startTimeShortString = String(substr)
        } else {
            self.startDate = Date()
            self.endDate = Date()
            self.startTimeShortString = "00:00"
        }
        
    }
    
}
