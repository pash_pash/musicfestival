//
//  AboutViewController.swift
//  MusicFestival
//
//  Created by Pasha on 6/1/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import UIKit

class AboutViewController: MFBaseVC {

    @IBOutlet weak var aboutTextView: UITextView!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutTextView.isScrollEnabled = false
        setupAboutText()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        aboutTextView.isScrollEnabled = true
    }
      
    func setupAboutText() {
        
        let attributedDesc = NSMutableAttributedString()
        
        //Part 1
        
        let aboutTitle1 = NSLocalizedString("About_Title_1", comment: "") + "\n\r"
        let aboutText1 = NSLocalizedString("About_Text_1", comment: "")
        
        let rangeTitle1: NSRange = NSRange(location: 0, length: aboutTitle1.count )
        let fontTitle1 = UIFont(name: "SFProText-Light", size: 36) ?? UIFont.systemFont(ofSize: 36)
        let attributedAboutTitle1: NSMutableAttributedString = NSMutableAttributedString(string: aboutTitle1)
        attributedAboutTitle1.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                             convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontTitle1]), range: rangeTitle1)
        
        let rangeText1: NSRange = NSRange(location: 0, length: aboutText1.count )
        let fontText1 = UIFont(name: "SFProText-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
        let attributedAboutText1: NSMutableAttributedString = NSMutableAttributedString(string: aboutText1)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        attributedAboutText1.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                             convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontText1,
                                             convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle): paragraphStyle]), range: rangeText1)
        
        
        //Part 2
        
        
        let aboutTitle2 = "\n\r" + NSLocalizedString("About_Title_2", comment: "") + "\n\r"
        let aboutText2 = NSLocalizedString("About_Text_2", comment: "")
        
        let rangeTitle2: NSRange = NSRange(location: 0, length: aboutTitle2.count )
        let fontTitle2 = UIFont(name: "SFProText-Light", size: 36) ?? UIFont.systemFont(ofSize: 36)
        let attributedAboutTitle2: NSMutableAttributedString = NSMutableAttributedString(string: aboutTitle2)
        attributedAboutTitle2.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                             convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontTitle2]), range: rangeTitle2)
        
        let rangeText2: NSRange = NSRange(location: 0, length: aboutText2.count )
        let fontText2 = UIFont(name: "SFProText-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
        let attributedAboutText2: NSMutableAttributedString = NSMutableAttributedString(string: aboutText2)
        attributedAboutText2.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                            convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontText2,
                                            convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle): paragraphStyle]), range: rangeText2)
        
        
        //Part 3
        
        
        let aboutTitle3 = "\n\r" + NSLocalizedString("About_Title_3", comment: "") + "\n\r"
        let aboutText3 = NSLocalizedString("About_Text_3", comment: "")
        
        let rangeTitle3: NSRange = NSRange(location: 0, length: aboutTitle3.count )
        let fontTitle3 = UIFont(name: "SFProText-Light", size: 36) ?? UIFont.systemFont(ofSize: 36)
        let attributedAboutTitle3: NSMutableAttributedString = NSMutableAttributedString(string: aboutTitle3)
        attributedAboutTitle3.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                             convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontTitle3]), range: rangeTitle3)
        
        let rangeText3: NSRange = NSRange(location: 0, length: aboutText3.count )
        let fontText3 = UIFont(name: "SFProText-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
        let attributedAboutText3: NSMutableAttributedString = NSMutableAttributedString(string: aboutText3)
        attributedAboutText3.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white,
                                            convertFromNSAttributedStringKey(NSAttributedString.Key.font): fontText3,
                                            convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle): paragraphStyle]), range: rangeText3)
        
        
        //Total
        
        
        attributedDesc.append(attributedAboutTitle1)
        attributedDesc.append(attributedAboutText1)
        attributedDesc.append(attributedAboutTitle2)
        attributedDesc.append(attributedAboutText2)
        attributedDesc.append(attributedAboutTitle3)
        attributedDesc.append(attributedAboutText3)
        
        
        self.aboutTextView.attributedText = attributedDesc
        
    }

    
    // MARK: Actions
    
    @IBAction func didTapFacebookButton(_ sender: Any) {
        if let url = URL(string: "https://m.facebook.com/fetedelamusique.lviv") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func didTapInsragramButton(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/fetedelamusiquelviv") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func didTapYoutubeButton(_ sender: Any) {
        if let url = URL(string: "https://www.youtube.com/user/fetedelamusiquelviv/videos") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
