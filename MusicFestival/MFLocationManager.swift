//
//  MFLocationManager.swift
//  MusicFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper

let kMFDidUpdateData = "MFdidUpdateData"



class MFLocationManager {
    static let sharedInstance = MFLocationManager()
    var locations: [MFLocation] = []
    var liveLocations: [MFLocation] = []
    var thirtyMinLocations: [MFLocation] = []
    var participants: [MFEvent] =  []
    
    var filteredLocations: [MFLocation] =  []
    var filteredParticipants: [MFEvent] =  []
    
    var selectedLocation: MFLocation!
    var selectedEvent: MFEvent!
    
    //var sortMode: SortMode = .SM_TIME
    var searchMode: Bool = false
    
    required init?() {
        
    }
    
    func updateData() {
        let url = URL(string: apiURL)!
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "GET" //set http method as POST
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    self.locations.removeAll()
                    if ((json["status"] as! String) == "ok") {
                        //print(json)
                        for (key, value) in json {
                            if (Int(key) != nil) {
                                let location:MFLocation = Mapper<MFLocation>().map(JSONObject: value)!
                                self.locations.append(location)
                                
                            }
                        }
                        self.sortLocationByName()
                        self.createTimeOnLocations()
                        self.createParticipants()
                        NotificationCenter.default.post(name: Notification.Name(kMFDidUpdateData), object: nil)
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    //MARK: Public
    func getLocationByParticipant(participant :MFEvent)-> MFLocation! {
        for location in self.locations {
            for event in location.events {
                if participant == event {
                    return location
                }
            }
        }
        return nil
    }
    
    func filterEvents(text: String) {
        self.filteredParticipants = self.participants.filter({ (event) -> Bool in
            guard let name = event.name else {
                return false
            }
            return (name.range(of: text) != nil)
        })
    }
    
    func filterLocations(text: String) {
        self.filteredLocations = self.locations.filter({ (location) -> Bool in
            guard let locationName = location.name else {
                return false
            }
            return (locationName.range(of: text) != nil)
        })
    }
    
    internal func sortLocationByName() {
        self.locations.sort { (loc1, loc2) -> Bool in
            return loc1.name < loc2.name
        }
    }
    
    internal func createTimeOnLocations() {
        //Live
        let nowDate: Date = Date()
        self.liveLocations.removeAll()
        for location in self.locations {
            for event in location.events {
                if let eventDate = event.date, eventDate.startDate >= nowDate && eventDate.endDate <= nowDate {
                    self.liveLocations.append(location)
                }
            }
        }
        //30 min
        let afterThirtyMinDate = nowDate.addingTimeInterval(30.0 * 60.0)
        self.thirtyMinLocations.removeAll()
        for location in self.locations {
            for event in location.events {
                if let eventDate = event.date, eventDate.startDate >= afterThirtyMinDate && eventDate.endDate <= afterThirtyMinDate {
                    self.thirtyMinLocations.append(location)
                }
            }
        }
    }
    
    internal func createParticipants() {
        self.participants.removeAll()
        for location in self.locations {
            for event in location.events {
                let location = self.getLocationByParticipant(participant: event)
                event.location = location
                self.participants.append(event)
            }
        }
    }

}
