//
//  Utilities.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 6/2/19.
//  Copyright © 2019 Pasha. All rights reserved.
//
import Foundation
import UIKit

class Utilities {
    
    static let sharedInstance = Utilities()
    
    func imageURLBy(lat: Double?, lng: Double?, frameWidth: Int, frameHeight: Int) -> URL? {
    
        var latitude: Double = DEF_LATITUDE
        var longitude: Double = DEF_LONGITUDE
        
        if lat != nil {
            latitude = lat!
        }
        
        if lng != nil {
            longitude = lng!
        }
        let staticMapUrl = "http://maps.google.com/maps/api/staticmap?markers=color:red|\(latitude),\(longitude)&\("zoom=18&size=\(frameWidth)x\(frameHeight)")&sensor=true&key=\(GM_API_KEY)"
        
        let url = URL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        return url
    }
    
    func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            
            
            let data = string.data(using: String.Encoding.unicode, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary([convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType): convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]),
                                                 documentAttributes: nil)
                
                return str
            }
        } catch {
            
        }
        return nil
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}
