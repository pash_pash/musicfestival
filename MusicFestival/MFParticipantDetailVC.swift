//
//  MFParticipantDetailVC.swift
//  MusicFestival
//
//  Created by Pasha on 6/8/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import UIKit
import SDWebImage
import Kanna
import WebKit

class ParticipantDescriptionCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var descrTextView: UITextView!
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        UIApplication.shared.open(URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        return false
    }

}

class ParticipantMediaCell: UITableViewCell, WKNavigationDelegate {
    //@IBOutlet weak var webView: UIWebView!
    //@IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var placeHolderView: MFEventPlaceholderView!
    @IBOutlet weak var mediaView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    func webView(_ webView: WKWebView, didFinish navigation:
        WKNavigation!) {
        self.placeHolderView.isHidden = true
        showActivityIndicator(show: false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation
        navigation: WKNavigation!) {
        self.placeHolderView.isHidden = false
        showActivityIndicator(show: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation:
        WKNavigation!, withError error: Error) {
        self.placeHolderView.isHidden = true
        showActivityIndicator(show: false)
    }
    
    func showActivityIndicator(show: Bool) {
        if show {
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        } else {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
    
    
}

class LocParticipantCell: UITableViewCell {
    @IBOutlet weak var participantImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}



class MFParticipantDetailViewController:MFBaseVC {
    
    @IBOutlet weak var participantName: UILabel!
    
    @IBOutlet weak var participantImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var participantImgViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var additionalImage: UIImageView!
    
    var participant: MFEvent?
    var location: MFLocation?
    var locParticipants: [MFEvent] = []
    var participantDescription = ""
    var mediaLinks: [String] = []
    var mediaCels: [ParticipantMediaCell] = []
    var mode: MFEventListMode = .SM_PARTICIPANT
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
 
    
    
    //Mark: Setup
    internal func setup() {
        if self.mode == .SM_PARTICIPANT {
            self.setupParticipantData()
            self.setupParticipantTopImageView()
        } else {
            self.setupLocationData()
            self.setupLocationTopImageView()
        }
        
        self.setupNavigationBar()
        self.view.layoutSubviews()
    }
    
    internal func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    internal func setupParticipantData() {
        self.participant = MFLocationManager.sharedInstance?.selectedEvent
        var resultDoc = ""
        let descr = self.participant?.descr ?? "No Information"
        if let doc = try? HTML(html: descr, encoding: .utf8) {
            resultDoc = doc.toHTML ?? ""
            // Search for nodes by CSS
            for link in doc.css("iframe, link") {
                resultDoc = resultDoc.replacingOccurrences(of: "<p>\(link.toHTML ?? "")</p>", with: "")
                self.mediaLinks.append(link["src"] ?? "")
            }
            
            if let docWithoutMedia = try? HTML(html: resultDoc, encoding: .utf8) {
                resultDoc = docWithoutMedia.body?.toHTML ?? ""
            }
        }
        self.participantDescription = resultDoc
    }
    
    internal func setupLocationData() {
        self.location = MFLocationManager.sharedInstance?.selectedLocation
    }
    
    
    internal func setupParticipantTopImageView() {
        let utilities = Utilities.sharedInstance
        let screenWidth = UIScreen.main.bounds.width
        
        self.participantName.font =  UIFont(name: "SFProText-Regular", size: screenWidth * 0.0533) ?? UIFont.systemFont(ofSize: screenWidth * 0.0533)
        self.locationLabel.font =  UIFont(name: "SFProText-Regular", size: screenWidth * 0.0373) ?? UIFont.systemFont(ofSize: screenWidth * 0.0373)
        
        let attrName: NSAttributedString! = utilities.stringFromHtml(string: self.participant?.name ?? "?")
        self.participantName.text = attrName.string
        
        
        if self.participant?.imagePath != nil {
            let url = URL(string: (self.participant!.imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
            self.participantImageView.sd_setImage(with: url)
            self.participantImgViewHeightConstraint.constant = 0.8 * screenWidth 
        } else {
            self.participantImgViewHeightConstraint.constant = 0
        }
        
        var locationName = self.participant?.location?.name ?? ""
        if locationName.count > 0 {
            locationName += ", "
        }
        locationName += self.participant?.date?.startTimeShortString ?? ""
        self.locationLabel.text = locationName
        self.additionalImage.isHidden = true
    }
    
    internal func setupLocationTopImageView() {
        
        let screenWidth = UIScreen.main.bounds.width
        self.additionalImage.isHidden = true //false
        let utilities = Utilities.sharedInstance
        var url = self.location?.imageURL
        if url == nil {
            url = utilities.imageURLBy(lat: self.location?.latlng.lat,
                                       lng: self.location?.latlng.lng,
                                       frameWidth: 2 * Int(self.participantImageView.bounds.size.width),
                                       frameHeight: 2 * Int(self.participantImageView.bounds.size.height))
            
        }
        
        self.participantImageView.sd_setImage(with: url)
        self.participantName.font =  UIFont(name: "SFProText-Regular", size: screenWidth * 0.0533) ?? UIFont.systemFont(ofSize: screenWidth * 0.0533)
        self.participantName.text = self.location?.name
        self.locationLabel.font =  UIFont(name: "SFProText-Regular", size: screenWidth * 0.0373) ?? UIFont.systemFont(ofSize: screenWidth * 0.0373)
        self.locationLabel.text = self.location?.place.address
    }
    
    
    //MARK: Actions
    func locButtonPressed() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "mapVCID") as! MFMapViewController
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    //MARK: IBActions
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapDirection(_ sender: Any) {
        
        guard let location = (self.mode == .SM_PARTICIPANT ? self.participant?.location : self.location) else {
            return
        }
        
        var lat: Double = 0.0
        var long: Double = 0.0
        
        if location.latlng.lat != nil {
            lat = location.latlng.lat
        } else {
            lat = DEF_LATITUDE
        }
        
        if location.latlng.lng != nil {
            long = location.latlng.lng
        } else {
            long = DEF_LONGITUDE
        }
        
        let strLat = String(lat)
        let strLong = String(long)
        var url: URL?
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            url = URL(string:"comgooglemaps://?saddr=&daddr=\(strLat),\(strLong)&directionsmode=driving")!
        }
        else {
            url = URL(string:"https://maps.google.com/?q=@\(strLat),\(strLong)")!
        }
        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        
    }
    
    
}

extension MFParticipantDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mode == .SM_PARTICIPANT {
            return 1 + self.mediaLinks.count
        } else {
            return self.location?.events.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mode == .SM_PARTICIPANT {
            return self.participantCell(tableView: tableView, indexPath: indexPath)
        } else {
            return self.locationCell(tableView: tableView, indexPath: indexPath)
        }
        
    }
    
    func participantCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let utilities = Utilities.sharedInstance
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "participantDescriptionCell") as! ParticipantDescriptionCell
            let descr = self.participantDescription
            let attributedText: NSMutableAttributedString = NSMutableAttributedString(attributedString: utilities.stringFromHtml(string: descr)!)
            
            //Add color atribute
            let range: NSRange = NSRange(location: 0, length: attributedText.length )
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
            //Add font (name, size) atribute
            let font = UIFont(name: "SFProText-Regular", size: 16)!
            attributedText.addAttribute(NSAttributedString.Key.font, value: font, range: range)
            cell.descrTextView.attributedText = attributedText
            cell.descrTextView.textAlignment = .left
            cell.descrTextView.sizeToFit()
            cell.descrTextView.linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue: UIColor.blue])
            return cell
        } else {
            if (indexPath.row - 1) < mediaCels.count {
                let cell = self.mediaCels[indexPath.row - 1]
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "participantMediaCell") as! ParticipantMediaCell
            cell.mediaView.navigationDelegate = cell
            let link = self.mediaLinks[indexPath.row - 1]
            let myURL = URL(string: link)
            let mediaRequest = URLRequest(url: myURL!)
            cell.mediaView.load(mediaRequest)
            self.self.mediaCels.append(cell)
            return cell
        }
    }
    
    func locationCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let utilities = Utilities.sharedInstance
        let cell: LocParticipantCell = tableView.dequeueReusableCell(withIdentifier: "locParticipantCell", for: indexPath) as! LocParticipantCell
        guard let loc = self.location else {
            return cell
        }
        let event = loc.events[indexPath.row]
        let url = URL(string: event.imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        cell.participantImage.sd_setImage(with: url)
        let attrName: NSAttributedString! = utilities.stringFromHtml(string: event.name)
        cell.nameLabel.text = attrName.string
        
        
        let eventTime = event.date?.startTimeShortString ?? ""
        cell.timeLabel.text = eventTime
        return cell
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
