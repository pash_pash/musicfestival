//
//  MFDot.swift
//  MusicFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper

class MFDot: NSObject, Mappable {
    var lat:Double!
    var lng:Double!
    
    override init() {
        super.init()
    }
    
    init(lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
        
    func mapping(map: Map) {
        var latStr: String!
        var lngStr: String!
        latStr     <- map["lat"]
        lngStr     <- map["lng"]
        if latStr != nil {
            lat = Double(latStr)
        } else {
            lat = DEF_LATITUDE
        }
        
        if lngStr != nil {
            lng = Double(lngStr)
        } else {
            lng = DEF_LONGITUDE
        }
    }

}
