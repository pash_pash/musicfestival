//
//  MFConstants.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 5/15/19.
//  Copyright © 2019 Pasha. All rights reserved.
//

import Foundation

let apiURL = "http://fetedelamusique.lviv.ua/api/events/get_all"
let DEF_LATITUDE: Double = 49.843092
let DEF_LONGITUDE: Double = 24.030923
let GM_API_KEY = "AIzaSyC76PELUuH1zvusc3s7buKVDvLSAcV1TQI"

public enum MFEventListMode: Int {
    case SM_PARTICIPANT = 0
    case SM_LOCATION = 1
}

public enum MFMapMode: Int {
    case MM_ALL = 0
    case MM_LIVE = 1
    case MM_30MIN = 2
}
