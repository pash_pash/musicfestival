//
//  MFEventPlaceholderView.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 5/16/19.
//  Copyright © 2019 Pasha. All rights reserved.
//

import UIKit

class MFEventPlaceholderView: UIView {
    
    override func draw(_ rect: CGRect) {
        let aPath = UIBezierPath()
        //Rectangle
        aPath.move(to: CGPoint(x:0, y:0))
        aPath.addLine(to: CGPoint(x:rect.width, y:0))
        aPath.addLine(to: CGPoint(x:rect.width, y:rect.height))
        aPath.addLine(to: CGPoint(x:0, y:rect.height))
        aPath.addLine(to: CGPoint(x:0, y:0))
        aPath.close()
        //Crossed Lines
        aPath.move(to: CGPoint(x:0, y:0))
        aPath.addLine(to: CGPoint(x:rect.width, y:rect.height))
        aPath.close()
        aPath.move(to: CGPoint(x:rect.width, y:0))
        aPath.addLine(to: CGPoint(x:0, y:rect.height))
        aPath.close()
        UIColor.crossedLinesEventPlaceholderColor.set()
        aPath.stroke()
    }
 

}
