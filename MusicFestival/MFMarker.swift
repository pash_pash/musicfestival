//
//  MFMarker.swift
//  MusicFestival
//
//  Created by Pasha on 6/10/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import GoogleMaps

class MFMarker: GMSMarker {
    var location: MFLocation!
}
