//
//  MFLocation.swift
//  MusucFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper

class MFLocation: NSObject, Mappable {
    var name:String!
    var events: [MFEvent]!
    var latlng: MFDot!
    var place: MFPlace!
    var imageURL:URL!
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let utils = Utilities.sharedInstance
        name    <- map["name"]
        if name == nil {
           name = NSLocalizedString("Unknown_place", comment: "")
        }
        events  <- map["events"]
        if events == nil {
            events = []
        }
        latlng  <- map["latlng"]
        if latlng == nil {
            latlng = MFDot(lat: DEF_LATITUDE, lng: DEF_LONGITUDE)
        }
        place <- map["place"]
        if place == nil {
            place = MFPlace(address: NSLocalizedString("Unknown_address", comment: ""),
                            city: NSLocalizedString("Unknown_city", comment: ""))
        }
        var htmlImageURL: String?
        htmlImageURL <- map["venue_img"]
        //print("\(name ?? "unknown place") - \(htmlImageURL ?? "wrong image url")")
        htmlImageURL = utils.stringFromHtml(string: htmlImageURL ?? "" )?.string
        htmlImageURL = htmlImageURL?.components(separatedBy: .whitespacesAndNewlines).joined()
        self.imageURL = URL(string: htmlImageURL ?? "")
    }
}
