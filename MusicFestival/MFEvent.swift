//
//  MFEvent.swift
//  MusicFestival
//
//  Created by Pasha on 6/4/17.
//  Copyright © 2017 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper


class MFEvent: NSObject, Mappable {
    var name: String!
    var descr: String!
    var imagePath: String!
    var date: MFEventDate?
    var location: MFLocation?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name        <- map["name"]
        if name == nil {
            name = NSLocalizedString("Unknown_participant", comment: "")
        }
        descr       <- map["desc"]
        if descr == nil {
            descr = ""
        }
        imagePath   <- map["img"]
        if imagePath == nil {
            imagePath = ""
        }
        date        <- map["date"]
        if date == nil {
            date = MFEventDate()
            date?.startDateString = "????-??-??"
            date?.startTimeString = "00:00"
            date?.endDateString = "????-??-??"
            date?.endTimeString = "00:00:00"
            date?.startDate = Date()
            date?.endDate = Date()
            date?.startTimeShortString = "00:00"
        }
       
    }
    
}
