//
//  MFEventVC.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 5/15/19.
//  Copyright © 2019 Pasha. All rights reserved.
//

import UIKit
import GoogleMaps



class MFEventCell: UITableViewCell {
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationTimeLabel: UILabel!
    var index: Int = 0
}

class MFEventVC: MFBaseVC {

    // MARK: - Variables
    @IBOutlet  var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var eventListSwitcher: UISegmentedControl!
    
    weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    //MARK: - Setup
    func setup () {
        self.setupSearchBar()
        self.setupEventListSwitcher()
        self.setupActivityIndicator()
    }
    
    func setupSearchBar() {
        self.searchBar.textField?.backgroundColor = UIColor.searchTextBackgroundColor
        self.searchBar.setPlaceholderText(color: UIColor.searchPlaceholder)
        self.searchBar.setSearchImage(color: UIColor.searchPlaceholder)
        
        
        self.searchBar.textField?.textColor = .white
        self.searchBar.delegate = self
    }
    
    func setupEventListSwitcher() {
        self.eventListSwitcher.clipsToBounds = true
        self.eventListSwitcher.layer.cornerRadius = 15 //whatever
        self.eventListSwitcher.layer.borderWidth = 1
        self.eventListSwitcher.layer.borderColor = self.eventListSwitcher.tintColor.cgColor
        let font = UIFont(name: "SFProText-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
        self.eventListSwitcher.setTitleTextAttributes([NSAttributedString.Key(rawValue: convertFromNSAttributedStringKey(NSAttributedString.Key.font)): font], for: .normal)
    }
    
    func setupActivityIndicator() {
        let activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        tableView.backgroundView = activityIndicatorView
        self.activityIndicatorView = activityIndicatorView
        self.activityIndicatorView.startAnimating()
    }
    
    // MARK: - Notification handlers
    override func didUpdatData() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
        }
    }

    @IBAction func didTapEventListSwitcher(_ sender: Any) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.setContentOffset(.zero, animated: false)
        }
    }
}


extension MFEventVC:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let locManager = MFLocationManager.sharedInstance
        let eventListMode = self.eventListSwitcher.selectedSegmentIndex
        if (locManager?.searchMode == false) {
            return eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue ? (locManager?.participants.count ?? 0) : (locManager?.locations.count ?? 0)
        } else {
            return eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue ? (locManager?.filteredParticipants.count ?? 0) :
                (locManager?.filteredLocations.count ?? 0)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let utilities = Utilities.sharedInstance
        let locManager = MFLocationManager.sharedInstance
        let cell: MFEventCell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! MFEventCell
        
        let screenWedth = UIScreen.main.bounds.width
        let nameFont =  UIFont(name: "SFProText-Regular", size: screenWedth * 0.0533) ?? UIFont.systemFont(ofSize: screenWedth * 0.0533)
        let descrFont =  UIFont(name: "SFProText-Regular", size: screenWedth * 0.0373) ?? UIFont.systemFont(ofSize: screenWedth * 0.0373)
        
        
        let eventListMode = self.eventListSwitcher.selectedSegmentIndex
        if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
            guard let participants = (locManager?.searchMode == false) ? locManager?.participants : locManager?.filteredParticipants else {
                return cell
            }
            if (indexPath.row > participants.count) {
                return cell
            }
            let event = participants[indexPath.row]
            let url = URL(string: event.imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            cell.eventImageView.sd_setImage(with: url)
            let attrName: NSAttributedString! = utilities.stringFromHtml(string: event.name)
            
            cell.nameLabel.font = nameFont
            cell.nameLabel.text = attrName.string
            
            var locationName = event.location?.name ?? ""
            if locationName.count > 0 {
                locationName += ", "
            }
            locationName += event.date?.startTimeShortString ?? ""
            cell.locationTimeLabel.font = descrFont
            cell.locationTimeLabel.text = locationName
            
        } else {
            guard let locations = (locManager?.searchMode == false) ? locManager?.locations : locManager?.filteredLocations else {
                return cell
            }
            if (indexPath.row > locations.count) {
                return cell
            }
            let location = locations[indexPath.row]
            let utilities = Utilities.sharedInstance
            var url = location.imageURL
            
            if url == nil {
                url = utilities.imageURLBy(lat: location.latlng.lat,
                                           lng: location.latlng.lng,
                                           frameWidth: 2 * Int(cell.bounds.size.width),
                                           frameHeight: 2 * Int(cell.bounds.size.height))
                
            }
            cell.eventImageView.sd_setImage(with: url)
            cell.index = indexPath.row
            cell.nameLabel.font = nameFont
            cell.nameLabel.text = location.name//.uppercased()
            cell.locationTimeLabel.font = descrFont
            cell.locationTimeLabel.text = location.place.address
            
            return cell
        }

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let locManager = MFLocationManager.sharedInstance
        tableView.deselectRow(at: indexPath, animated: true)
        let eventListMode = self.eventListSwitcher.selectedSegmentIndex
        if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
            guard let event = (locManager?.searchMode)! ? locManager?.filteredParticipants[indexPath.row] : locManager?.participants[indexPath.row] else {
                return
            }
            locManager?.selectedEvent = event
        } else {
            guard let location = (locManager?.searchMode)! ? locManager?.filteredLocations[indexPath.row] : locManager?.locations[indexPath.row] else {
                return
            }
            locManager?.selectedLocation = location
        }
        self.performSegue(withIdentifier: "eventDetailSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        return 0.8 * screenWidth
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetailSegue" {
            let destVC: MFParticipantDetailViewController = segue.destination as! MFParticipantDetailViewController
            let eventListMode = self.eventListSwitcher.selectedSegmentIndex
            if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
                destVC.mode = .SM_PARTICIPANT
            } else {
                destVC.mode = .SM_LOCATION
            }
        }
    }
    
}

extension MFEventVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let locManager = MFLocationManager.sharedInstance
        let searchText = searchBar.text
        if searchText != nil {
            if searchText!.count == 0 {
                searchBar.setShowsCancelButton(true, animated: true)
                let eventListMode = self.eventListSwitcher.selectedSegmentIndex
                if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
                    locManager?.filteredParticipants = []
                } else {
                    locManager?.filteredLocations = []
                }
            }
        }

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let locManager = MFLocationManager.sharedInstance
        let eventListMode = self.eventListSwitcher.selectedSegmentIndex
        if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
            locManager?.filterEvents(text: searchText)
        } else {
            locManager?.filterLocations(text: searchText)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        if locManager?.searchMode == false && searchText.count > 0 {
            locManager?.searchMode = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let locManager = MFLocationManager.sharedInstance
        if searchBar.text?.count == 0 {
            locManager?.searchMode = false
        } else {
            let eventListMode = self.eventListSwitcher.selectedSegmentIndex
            if eventListMode == MFEventListMode.SM_PARTICIPANT.rawValue {
                locManager?.filterEvents(text: searchBar.text!)
            } else {
                locManager?.filterLocations(text: searchBar.text!)
            }
        }
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let locManager = MFLocationManager.sharedInstance
        searchBar.text = ""
        searchBar.resignFirstResponder()
        locManager?.searchMode = false
        searchBar.setShowsCancelButton(false, animated: true)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
