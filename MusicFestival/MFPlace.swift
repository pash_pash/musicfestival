//
//  MFPlace.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 6/4/19.
//  Copyright © 2019 Pasha. All rights reserved.
//

import Foundation
import ObjectMapper

class MFPlace: NSObject, Mappable {
    var address: String!
    var city: String!
    
    override init() {
        super.init()
    }
    
    init(address: String, city: String) {
        self.address = address
        self.city = city
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        address    <- map["address"]
        if address == nil {
            address = NSLocalizedString("Unknown_address", comment: "")
        }
        city  <- map["city"]
        if city == nil {
            city = NSLocalizedString("Unknown_city", comment: "")
        }
    }
}
