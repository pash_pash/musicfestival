//
//  MFBaseVC.swift
//  MusicFestival
//
//  Created by Pavlo Pashenko on 5/16/19.
//  Copyright © 2019 Pasha. All rights reserved.
//

import UIKit

class MFBaseVC: UIViewController {

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(MFBaseVC.didUpdatData), name: Notification.Name(kMFDidUpdateData), object: nil)
        if MFLocationManager.sharedInstance!.locations.count < 1 {
            MFLocationManager.sharedInstance?.updateData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Notification handlers
    @objc func didUpdatData() {
        
    }
    
}
